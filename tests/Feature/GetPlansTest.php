<?php

namespace Tests\Feature;

use App\Models\Plan;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GetPlansTest extends TestCase
{
    use RefreshDatabase;

    public function testGetPlansRoute()
    {
        $plans = factory(Plan::class, 3)->create();
        $response = $this->json('get', route('get_plans'));
        $response
            ->assertStatus(200)
            ->assertExactJson($plans->toArray());
    }
}
