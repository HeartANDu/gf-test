<?php

namespace Tests\Feature;

use App\Models\Client;
use App\Models\Order;
use App\Models\Plan;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Tests\TestCase;

class CreateOrderTest extends TestCase
{
    use RefreshDatabase;

    const FORMAT = 'D M d Y';

    /** @var Collection */
    private $plans;
    /** @var Generator */
    private $faker;
    /** @var array */
    private $data;

    protected function setUp(): void
    {
        parent::setUp();
        $this->plans = factory(Plan::class, 3)->create([
            'delivery_days' => ['mon', 'tue', 'wed', 'thu', 'fri'],
        ]);
        $this->faker = resolve(Generator::class);
        $this->data = [
            'phone' => '+79998887766',
            'name' => $this->faker->name,
            'plan' => $this->plans[$this->faker->numberBetween(0, 2)]->id,
            'delivery_date' => $this->faker
                ->dateTimeInInterval('Monday next week', '+5 days')
                ->format(self::FORMAT),
            'delivery_address' => $this->faker->address,
        ];
    }

    public function incorrectPhoneProvider()
    {
        return [
            ['89182754209'],
            ['+7 999-999-99-99'],
            ['8-999-999-99-99'],
            ['8 (495) 882-99-00'],
        ];
    }

    public function incorrectNameProvider()
    {
        return [
            [''],
            ['Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'],
        ];
    }

    public function incorrectDeliveryAddressProvider()
    {
        return [
            [''],
            ['Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.']
        ];
    }

    public function testSuccessfulOrderCreation()
    {
        $response = $this->json('post', route('create_order'), $this->data);
        $response->assertStatus(200);
        $order = resolve(Order::class)->orderBy('id', 'desc')->first();
        $response->assertExactJson(['order_id' => $order->id]);
    }

    /**
     * @depends testSuccessfulOrderCreation
     */
    public function testSuccessfulMultipleOrdersCreationByOneClient()
    {
        $response = $this->json('post', route('create_order'), $this->data);
        $response->assertStatus(200);
        $this->data['name'] = $this->faker->name;
        $this->data['plan'] = $this->plans[$this->faker->numberBetween(0, 2)]->id;
        $response = $this->json('post', route('create_order'), $this->data);
        $response->assertStatus(200);
        $client = resolve(Client::class)
            ->where(
                'phone',
                preg_replace('/[^0-9]/', '', $this->data['phone'])
            )
            ->get();
        $this->assertCount(1, $client);
        $this->assertEquals($this->data['name'], $client->first()->name);
    }

    public function testFailedOrderBeforeTomorrow()
    {
        $this->data['delivery_date'] = $this->faker
            ->dateTimeInInterval('Monday last week', '+5 days')
            ->format(self::FORMAT);
        $response = $this->json('post', route('create_order'), $this->data);
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors('delivery_date');
    }

    public function testFailedOrderOutsideOfPlanDeliveryDays()
    {
        $this->data['delivery_date'] = $this->faker
            ->dateTimeInInterval('Saturday next week', '+2 days')
            ->format(self::FORMAT);
        $response = $this->json('post', route('create_order'), $this->data);
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors('delivery_date');
    }

    /**
     * @dataProvider incorrectPhoneProvider
     */
    public function testFailedOrderWithIncorrectPhone($phone)
    {
        $this->data['phone'] = $phone;
        $response = $this->json('post', route('create_order'), $this->data);
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors('phone');
    }

    /**
     * @dataProvider incorrectNameProvider
     */
    public function testFailedOrderWithIncorrectName($name)
    {
        $this->data['name'] = $name;
        $response = $this->json('post', route('create_order'), $this->data);
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors('name');
    }

    public function testFailedOrderWithNotExistingPlan()
    {
        $this->data['plan'] = $this->plans->count() + 2;
        $response = $this->json('post', route('create_order'), $this->data);
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors('plan');
    }

    /**
     * @dataProvider incorrectDeliveryAddressProvider
     */
    public function testFaildedOrderWithIncorrectDeliveryAddress($address)
    {
        $this->data['delivery_address'] = $address;
        $response = $this->json('post', route('create_order'), $this->data);
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors('delivery_address');
    }
}
