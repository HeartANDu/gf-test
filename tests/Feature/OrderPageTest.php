<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderPageTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testOrderPage()
    {
        $response = $this->get('/');
        $response
            ->assertSeeText(__('main.order_page_title'))
            ->assertStatus(200);
    }
}
