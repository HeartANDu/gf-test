<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'name' => 'Недорогой',
            'price' => 999.99,
            'delivery_days' => '["mon","wed","fri"]',
        ]);
        DB::table('plans')->insert([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'name' => 'Улучшенный',
            'price' => 4999.99,
            'delivery_days' => '["mon","tue","wed","thu","fri"]',
        ]);
        DB::table('plans')->insert([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'name' => 'Премиум',
            'price' => 19999.99,
            'delivery_days' => '["mon","tue","wed","thu","fri","sat","sun"]',
        ]);
    }
}
