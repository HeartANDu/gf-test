<?php

use App\Models\Plan;
use Faker\Generator as Faker;

$factory->define(Plan::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'price' => $faker->randomFloat(2),
        'delivery_days' => $faker->randomElements(
            ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'],
            $faker->numberBetween(1, 7)
        )
    ];
});
