FROM php:7.2

ENV PYTHONUNBUFFERED=1 DOCKER=1 DEBIAN_FRONTEND=noninteractive

# Add support for apt-* packages caching through "apt-cacher-ng"
ARG APTPROXY
RUN bash -c 'if [ -n "$APTPROXY" ]; then echo "Acquire::HTTP::Proxy \"http://$APTPROXY\";" > /etc/apt/apt.conf.d/01proxy; fi'

## Install dependencies
RUN apt-get update \
    && apt-get --no-install-recommends install -y vim-tiny git unzip wget libxml2-dev vim
RUN pecl install xdebug \
    && docker-php-ext-install pdo pdo_mysql soap pcntl \
    && docker-php-ext-enable xdebug \
    && curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
    && curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
    && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" \
    && php /tmp/composer-setup.php \
    && php -r "unlink('/tmp/composer-setup.php');" \
    && mv composer.phar /usr/local/bin/composer

WORKDIR /code

ADD ./database /code/database
ADD ./composer.json /code/composer.json
RUN composer install --no-scripts
VOLUME /code/vendor
COPY ./xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
COPY ./.env.example /code/.env
COPY . /code

EXPOSE 8000
