## Тестовое задание GF

http://localhost:8000

### Запуск окружения
```bash
docker-compose build
docker-compose up -d
```

### Первый запуск
```bash
cp .env.example .env
docker-compose exec web bash -c "php artisan key:generate --ansi; php artisan migrate; php artisan db:seed"
```

### Запуск тестов
```bash
docker-compose run --rm web php vendor/bin/phpunit
```
или
```bash
docker-compose exec web php vendor/bin/phpunit
```
