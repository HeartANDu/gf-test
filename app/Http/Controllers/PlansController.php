<?php

namespace App\Http\Controllers;

use App\Models\Plan;

class PlansController
{
    /**
     * Получение списка всех тарифов
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getPlans()
    {
        return response(Plan::all());
    }
}
