<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderRequest;
use App\Models\Client;
use App\Models\Order;
use App\Models\Plan;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Роут для создания заказа
     *
     * @param CreateOrderRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function create(CreateOrderRequest $request)
    {
        $input = $request->validated();
        /** @var Plan $plan */
        $plan = Plan::find($input['plan']);
        $delivery_date = Carbon::parse($input['delivery_date']);
        $weekday = strtolower($delivery_date->format('D'));
        if (!collect($plan->delivery_days)->contains($weekday)) {
            return $this->makeDeliveryDateErrorResponse('delivery_date_not_in_plan');
        }

        if ($delivery_date <= Carbon::today()) {
            return $this->makeDeliveryDateErrorResponse('delivery_date_before_tomorrow');
        }

        $phone = preg_replace('/[^0-9]/', '', $input['phone']);
        DB::beginTransaction();
        /** @var Client|null $client */
        $client = Client::lockForUpdate()->firstOrNew(['phone' => $phone]);
        $client->name = $input['name'];
        $client->save();

        /** @var Order $order */
        $order = resolve(Order::class);
        $order->client_id = $client->id;
        $order->plan_id = $plan->id;
        $order->start_day = $delivery_date;
        $order->delivery_address = $input['delivery_address'];
        $order->save();

        DB::commit();

        return response(['order_id' => $order->id]);
    }

    /**
     * Генерация ответа с ошибкой
     *
     * @param string $lang_key
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    private function makeDeliveryDateErrorResponse(string $lang_key)
    {
        return response([
            'message' => 'The given data was invalid.',
            'errors' => [
                'delivery_date' => __(
                    'validation.' . $lang_key,
                    ['attribute' => __('main.delivery_date_field')]
                )
            ],
        ], 422);
    }
}
