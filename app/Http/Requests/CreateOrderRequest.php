<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => ['required', 'string', 'regex:/^\+?7\d{10}$/'],
            'name' => 'required|string|max:100',
            'plan' => 'required|exists:plans,id',
            'delivery_date' => 'required|date',
            'delivery_address' => 'required|string|max:255',
        ];
    }

    public function attributes()
    {
        return [
            'phone' => __('main.phone_field'),
            'name' => __('main.name_field'),
            'plan' => __('main.plan_field'),
            'delivery_date' => __('main.delivery_date_field'),
            'delivery_address' => __('main.delivery_address_field'),
        ];
    }
}
