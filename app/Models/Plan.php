<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $casts = [
        'price' => 'float',
        'delivery_days' => 'array',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class, 'plan_id', 'id');
    }
}
