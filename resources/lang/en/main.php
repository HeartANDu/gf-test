<?php

return [
    'order_page_title' => 'Make an order',
    'phone_field' => 'phone',
    'name_field' => 'name',
    'plan_field' => 'plan',
    'delivery_date_field' => 'Delivery date',
    'delivery_address_field' => 'delivery address',
];
