<?php

return [
    'order_page_title' => 'Оформить заказ',
    'phone_field' => 'номер телефона',
    'name_field' => 'имя',
    'plan_field' => 'тариф',
    'delivery_date_field' => 'дата доставки',
    'delivery_address_field' => 'адрес доставки',
];
