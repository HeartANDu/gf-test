<?php

return [
    'date' => ':attribute не является корректной датой.',
    'delivery_date_not_in_plan' => 'Поле :attribute не входит в дни доставки выбранного тарифа.',
    'delivery_date_before_tomorrow' => 'Поле :attribute нельзя выбрать раньше, чем завтра.',
    'exists' => 'Поле :attribute некорректно.',
    'max' => [
        'string' => 'Поле :attribute должно быть меньше :max символов.',
    ],
    'regex' => 'Неверный формат поля :attribute.',
    'required' => 'Поле :attribute обязательно для заполнения.',
    'string' => 'Поле :attribute должно быть строкой.',
];
